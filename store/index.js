import {createStore} from 'vuex'

const store = createStore({
	state:{
		hourA:'',
		hourB:'',
		minuteA:'',
		minuteB:''
	},
	getters:{
		hourA(state) {
			return state.hourA;
		},
		hourB(state) {
			return state.hourB;
		},
		minuteA(state) {
			return state.minuteA;
		},
		minuteB(state) {
			return state.minuteB;
		}
	},
	mutations: {
		hourA(state, obj) {
			//console.log(obj);
			state.hourA = obj.hourA
		},
		hourB(state, obj) {
			state.hourB = obj.hourB
		},
		minuteA(state, obj) {
			state.minuteA = obj.minuteA
		},
		minuteB(state, obj) {
			state.minuteB = obj.minuteB
		}
	}
})

export default store