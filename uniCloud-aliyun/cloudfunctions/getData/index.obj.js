// 云对象教程: https://uniapp.dcloud.net.cn/uniCloud/cloud-obj
// jsdoc语法提示教程：https://ask.dcloud.net.cn/docs/#//ask.dcloud.net.cn/article/129
const db = uniCloud.database();
module.exports = {
	_before: function () { // 通用预处理器

	},
	/**
	 * method1方法描述
	 * @param {string} param1 参数1描述
	 * @returns {object} 返回值描述
	 */
	/* 
	method1(param1) {
		// 参数校验，如无参数则不需要
		if (!param1) {
			return {
				errCode: 'PARAM_IS_NULL',
				errMsg: '参数不能为空'
			}
		}
		// 业务逻辑
		
		// 返回结果
		return {
			param1 //请根据实际需要返回值
		}
	}
	*/
   async getTextData(){
	  const textData = await db.collection('textdata').where({_id:"65e6dab2337a9fefccf85aec"}).get() 
	  return textData.data[0].content
   },
   async getSlogan(){
   	  const slogan = await db.collection('slogan').where({_id:"65e6c4b4bd022087dfabc5d1"}).get() 
   	  return slogan.data[0]
   },
   async getMyData(){
   	  const mydata = await db.collection('user').where({_id:"65e6c6c799c6244dcfbfa520"}).get() 
   	  return mydata.data[0]
   },
   async getAbout(){
   	  const about = await db.collection('skills').where({_id:"65e6d358652341ed5e13cdf9"}).get() 
   	  return about.data[0]
   }
}
